(function($){
var $allPoints = $('td.allPoints');
var $allCredits = $('input[name="credit"], select[name="credit"]');

$('body').on('click','button', function(e){
   var grade = $(e.currentTarget).parent('td').parent('tr').find('input[name="grade"], select[name="grade"]').val(); 
   var credit = $(e.currentTarget).parent('td').parent('tr').find('input[name="credit"], select[name="credit"] option:selected').val();
   var gpa = $(e.currentTarget).parent('td').parent('tr').find('input[name="gpa"]').val();
 
   var cumulative = totalPoints(parseFloat(gpa), parseFloat(credit));
   $(e.currentTarget).parent('td').parent('tr').find('#cumulativeQualityPoints').text(cumulative);
console.log(parseFloat(credit));
   var total = totalPoints(parseFloat(computeGradeNum(grade), 10), parseFloat(credit, 10));
   $(e.currentTarget).parent('td').parent('tr').find('#qualityPoints').text(total);
   totalOnChange($('#newCumulativeQualityPoints'), $allPoints)
  overallGPA();
});


  $('table').on('change', 'input[name="credit"], select[name="credit"]', totalOnChange.bind(this, $('#newCumulativeCredits'), $allCredits));
//  $('td').on('click', $allPoints, totalOnChange.bind(this, $('#newCumulativeQualityPoints'), $allPoints));

//totalOnChange for calculation of columns
function totalOnChange(ele, col){
 var total = 0;
  var creditsPointsValue;
 // console.log(col);
  col.each(function(){
//console.log($(this).val());
    if($(this).is('input') || $(this).is('select')){
      creditsPointsValue = parseFloat($(this).val());
    }else{
      creditsPointsValue = parseFloat($(this).text());
    }
console.log(creditsPointsValue);
    if(!isNaN(creditsPointsValue)) {
      total += creditsPointsValue; 
    }
  });

  ele.text(total);  
  
  }
//totalPoints for calculation of rows
function totalPoints(gpaOrLetGrade, credits) {
  return credits * gpaOrLetGrade;
}
function overallGPA(){
  var overallCredits = parseFloat($('#newCumulativeCredits').text(), 10);
  var overallPoints = parseFloat($('#newCumulativeQualityPoints').text(), 10);
  
  var overall =  overallPoints / overallCredits;
  $('#newCumulativeGpa').text(overall.toFixed(4));
}
function computeGradeNum(input){

  var gradenum = 0;
  var thegrade=input;
  if (thegrade==="A" || thegrade==="a") {gradenum=4;}
  else if (thegrade==="B" || thegrade==="b") {gradenum=3;}
  else if (thegrade==="C" || thegrade==="c") {gradenum=2;}
  else if (thegrade==="D" || thegrade==="d") {gradenum=1;}
  else if (thegrade==="B+" || thegrade==="b+") {gradenum=3.3;}
  else if (thegrade==="C+" || thegrade==="c+") {gradenum=2.3;}
  else if (thegrade==="D+" || thegrade==="d+") {gradenum=1.3;}
  else if (thegrade==="A-" || thegrade==="a-") {gradenum=3.7;}
  else if (thegrade==="B-" || thegrade==="b-") {gradenum=2.7;}
  else if (thegrade==="C-" || thegrade==="c-") {gradenum=1.7;}
  else if (thegrade==="D-" || thegrade==="d-") {gradenum=.7;}
  else if (thegrade==="F" || thegrade==="f") {gradenum=0;}

  
  return gradenum;
     }

})(jQuery);